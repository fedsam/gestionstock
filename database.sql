/*
	Import this file with following command :
	mysql -u root -p gestionstock < database.sql --default-character-set=utf8mb4
*/

DROP TABLE IF EXISTS RAYON;
CREATE TABLE RAYON(id INT PRIMARY KEY AUTO_INCREMENT, nom VARCHAR(20));

DROP TABLE IF EXISTS ROLE;
CREATE TABLE ROLE(id INT PRIMARY KEY AUTO_INCREMENT, nom VARCHAR(20));

DROP TABLE IF EXISTS MAGASIN;
CREATE TABLE MAGASIN(id INT PRIMARY KEY AUTO_INCREMENT , nom VARCHAR(20),ville VARCHAR(20));

DROP TABLE IF EXISTS USERS;
CREATE TABLE USERS (id INT PRIMARY KEY AUTO_INCREMENT,nom VARCHAR(20), prenom VARCHAR(20),email VARCHAR(50),password VARCHAR(50),id_role INT REFERENCES Role(id), id_rayon INT DEFAULT NULL REFERENCES Rayon(id));

DROP TABLE IF EXISTS ARTICLE;
CREATE TABLE ARTICLE(id INT PRIMARY KEY AUTO_INCREMENT, reference VARCHAR(20) NOT NULL UNIQUE,nom VARCHAR(50),Qte INT,Prix DOUBLE, id_rayon INT REFERENCES Rayon(id), id_magasin INT REFERENCES Magasin(id) ); 

INSERT INTO RAYON VALUES(NULL,"Cyclotourisme"),(NULL,"Pêche"),(NULL,"Randonnée"),(NULL,"Tennis"),(NULL,"Camping"),(NULL,"Fitness");

INSERT INTO ROLE VALUES (NULL,"Utilisateur"),(NULL,"Administrateur"),(NULL,"SuperAdmin");

INSERT INTO MAGASIN VALUES(NULL,"Décathlon","Chambray-lès-Tours"),(NULL,"Décathlon","Tours Nord");

INSERT INTO USERS VALUES(NULL,"admin","admin","admin@admin.com","Adminapp",3,NULL);

INSERT INTO USERS VALUES(NULL,"jean","dupont","jean@dupont.com","Userapp",1,NULL);

INSERT INTO ARTICLE VALUES(NULL,"1A","Cuissard",10,40.0,1,1),(NULL,"1B","Casque vélo Gris",20,35.0,1,1),(NULL,"1C","Chaussures vélo route",40,75.0,1,1),
		   (NULL,"2A","Canne à pêche",10,150,2,1),(NULL,"2B","Bouillette spécial carpe",20,10.0,2,1),(NULL,"2C","Moulinet",40,30.0,2,1),
		   (NULL,"3A","Chaussures de randonnée montagne homme",10,15.0,3,1),(NULL,"3B","Chaussures de randonnée montagne femme",20,45.0,3,1),(NULL,"3C","Chaussures de randonnée montagne enfant",40,30.0,3,1),
	 	   (NULL,"4A","Raquette",10,15.0,4,1),(NULL,"4B","Balle de Tennis",20,5.0,4,1),(NULL,"4C","Chaussure enfant tennis",40,18.0,4,1),
		   (NULL,"5A","Tente de camping à arceaux",10,450.0,5,1),(NULL,"5B","Tente de trekking",20,150.0,5,1),(NULL,"5C","Tente de camping gonflable",40,80.0,5,1),
		   (NULL,"6A","Vélo d’appartement",10,300.0,6,1),(NULL,"6B","Brassière Fitness",20,15.0,6,2),(NULL,"6C","Veste fitness",40,58.0,6,2);	




