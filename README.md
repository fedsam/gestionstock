# gestionStock

## Installation

L’installation de [Node.JS](https://nodejs.org/en/)  est nécessaire.

### Base de données
Il faut également une base de données MySQL et saisir les commandes suivantes :
```sql
create user 'utilisateur' identified with mysql_native_password by 'mot_de_passe';
grant all privileges on *.* to 'utilisateur'@'%';
flush privileges;
create database gestionstock character set utf8mb4 collate utf8mb4_0900_ai_ci;
```

Saisir cette commande pour importer la base de données du projet :
```bash
$ mysql -u root -p gestionstock < database.sql --default-character-set=utf8mb4
```

### Application Web

Afin d’installer toutes les dépendances il faut saisir la commande :
```bash
$ npm install
```

Il faut à présent créer un fichier .env à la racine du projet qui suit ce format : 
```
HOSTDB=adresse IP de la bdd (localhost par défaut)
PORTDB=Port de la bdd
USERDB=utilisateur précédemment créé
PSSWD=Mot de passe de l’utilisateur précédemment créé
DBNAME=gestionstock
PORT=Port de l’application Web voulu
SESS_SECRET=Chaine de caractères par exemple : secret
```

## Lancement

Pour lancer l’application Web saisir :
```bash
$ npm start
```

Pour lancer les tests saisir :
```bash
$ npm test
```