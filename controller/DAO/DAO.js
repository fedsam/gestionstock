class DAO {
    constructor() {
        if (new.target === DAO) {
            throw new TypeError("Cannot construct Abstract class")
        }
    }

    find() {
        throw new Error("Must be implemented")
    }
    
    create() {
        throw new Error("Must be implemented")
    }  

    update() {
        throw new Error("Must be implemented")
    }  

    delete() {
        throw new Error("Must be implemented")
    }
}

module.exports = DAO