'use strict'

const ShopDAO = require('./DAO/ShopDAO'),
	SectionDAO = require('./DAO/SectionDAO'),
	ItemDAO = require('./DAO/ItemDAO')

exports.loginController = (req, res) => {
	let options = { title: 'Connexion' }

	if (req.query.error === 'ID') options.error_id = true
	res.render('login', options)
}

exports.dashboardController = async (req, res) => {
	console.log(req.session.user.id_rayon !== null)
	let options = {
		title: 'Accueil',
		admin: req.session.user.id_role > 1 ? true : false,
		sectionChief: req.session.user.id_rayon !== null ? true : false
	}
	let shopDAO = new ShopDAO(),
		shopsArray = [],
		sectionDAO  = new SectionDAO(),
		sectionsArray = [],
		itemDAO = new ItemDAO(),
		itemsArray = []

	await shopDAO.getAll().then((shops) => {
		shops.forEach(element => shopsArray.push(element))
	})

	await sectionDAO.getAll().then((sections) => {
		sections.forEach(element => sectionsArray.push(element))
	})

	await itemDAO.getAll().then((articles) => {
		articles.forEach((element) => itemsArray.push(element))
	})

	options.shops = shopsArray
	options.sections = sectionsArray
	options.articles = itemsArray

	res.render('dashboard', options)
}
