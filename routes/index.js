'use strict'

const express = require('express'),
	controller = require('../controller/controller'),
	{ redirectLogin, redirectLogged } = require('./../middlewares/login'),
	router = express.Router()

router.get('/', redirectLogged, controller.loginController)
router.get('/dashboard', redirectLogin, controller.dashboardController)

module.exports = router
