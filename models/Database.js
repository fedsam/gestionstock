'use strict'

const mysql = require('mysql')

const pool = mysql.createPool({
	host: process.env.HOSTDB,
	port: process.env.PORTDB,
	user: process.env.USERDB,
	password: process.env.PSSWD,
	database: process.env.DBNAME,
	charset: 'utf8'
})

module.exports = pool
