describe('Administration Test', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/')
        cy.get('input[name=email]').type("admin@admin.com")
        cy.get('input[name=password]').type(`${'admin'}{enter}`)
        cy.get('a[href="/admin"]').click()
      })
    it('Administration : TA01', () => {
     
      cy.url().should('eq', 'http://localhost:3000/admin')
      
    })
    it('Administration : TA02', () => {
        cy.get('a[href="/admin/edit/1/?name=admin&firstname=admin&email=admin@admin.com&role=3&section=null"').click()
        cy.url().should('eq','http://localhost:3000/admin/edit/1/?name=admin&firstname=admin&email=admin@admin.com&role=3&section=null')
     
    })
    it('Administration : TA03', () => {
        cy.get('a[href="/admin/create"]').click()
        cy.url().should('eq','http://localhost:3000/admin/create')
    })
    it('Administration : TA04', () => {
        cy.get('a[href="/admin/edit/2/?name=jean&firstname=dupont&email=jean@dupont.com&role=1&section=1"]').click()
        cy.get('input[name=name]').clear().type("guillaume")
        cy.get('button[class="btn btn-block btn-success"]').click()
        cy.url().should('eq','http://localhost:3000/admin')
        cy.get('a[href="/admin/edit/2/?name=guillaume&firstname=dupont&email=jean@dupont.com&role=1&section=1"]').click()
        cy.get('input[name=name]').clear().type("jean")
        cy.get('button[class="btn btn-block btn-success"]').click()
    })
    it('Administration : TA05', () => {
        cy.get('a[href="/admin/edit/2/?name=jean&firstname=dupont&email=jean@dupont.com&role=1&section=1"]').click()
        cy.get('input[name=name]').type(" ")
        cy.get('button[class="btn btn-block btn-success"]').click()
        cy.url().should('include','/admin/edit/2/?name=jean&firstname=dupont&email=jean@dupont.com&role=1&section=1')
        cy.get('div[class="alert alert-danger mt-2"]').should('be.visible')
    })
    it('Administration : TA06', () => {
        cy.get('a[href="/admin/edit/2/?name=jean&firstname=dupont&email=jean@dupont.com&role=1&section=1"]').click()
        cy.get('input[name=name]').type("123")
        cy.get('button[class="btn btn-block btn-success"]').click()
        cy.url().should('include','/admin/edit/2/?name=jean&firstname=dupont&email=jean@dupont.com&role=1&section=1')
        cy.get('div[class="alert alert-danger mt-2"]').should('be.visible')
    })
    it('Administration : TA07', () => {
        cy.get('a[href="/admin/create"]').click()
        cy.get('input[name=name]').type("sean")
        cy.get('input[name=firstname]').type("anica")
        cy.get('input[name=email]').type("sean@anica.com")
        cy.get('input[name=password]').type("seanAnica")
        cy.get('button[class="btn btn-block btn-success"]').click()
        cy.url().should('eq','http://localhost:3000/admin')
    })
    it('Administration : TA08', () => {
        
        cy.get('a[class="btn btn-block btn-success"]').last().click()
        cy.get('button[class="btn btn-block btn-danger"]').click()
        cy.url().should('eq','http://localhost:3000/admin')
    })
    it('Administration : TA09', () => {
        cy.get('a[href="/admin/create"]').click()
     
        cy.get('input[name=name]').type("sean")
        cy.get('input[name=firstname]').type(" ")
        cy.get('input[name=email]').type("sean@anica.com")
        cy.get('input[name=password]').type("seanAnica")
        cy.get('button[class="btn btn-block btn-success"]').click()
        cy.url().should('include','http://localhost:3000/admin/create')
        cy.get('div[class="alert alert-danger mt-2"]').should('be.visible')
    })
    it('Administration : TA10', () => {
        cy.get('a[href="/admin/create"]').click()
     
        cy.get('input[name=name]').type("sean")
        cy.get('input[name=firstname]').type(123)
        cy.get('input[name=email]').type("sean@anica.com")
        cy.get('input[name=password]').type("seanAnica")
        cy.get('button[class="btn btn-block btn-success"]').click()
        cy.url().should('include','http://localhost:3000/admin/create')
        cy.get('div[class="alert alert-danger mt-2"]').should('be.visible')
    })
  })