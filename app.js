const express = require('express'),
	compression = require('compression'),
	bodyParser = require('body-parser'),
	session = require('express-session'),
	MySQLStore = require('express-mysql-session')(session),
	morgan = require('morgan'),
	helmet = require('helmet'),
	hpp = require('hpp'),
	app = express()

// ENV Vars
require('dotenv/config')

// View Engine
app.set('view engine', 'pug')

// Static files
app.use('/static', express.static(__dirname + '/public'))

// Config session store
const sessionStore = new MySQLStore({
	host: process.env.HOSTDB,
	port: process.env.PORTDB,
	user: process.env.USERDB,
	password: process.env.PSSWD,
	database: process.env.DBNAME,
})

// Imports routes
const indexRoutes = require('./routes/index')
	usersRoutes = require('./routes/users'),
	adminRoutes = require('./routes/admin'),
	itemRoutes = require('./routes/item')

// Middlewares
//app.use(morgan('dev'))
app.use(helmet())
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(hpp())
app.use(
	session({
		name: 'session',
		resave: false,
		saveUninitialized: false,
		secret: process.env.SESS_SECRET,
		store: sessionStore,
		cookie: {
			maxAge: 7200000,
			sameSite: true
			// secure: true
		}
	})
)

// Routes
app.use('/', indexRoutes)
app.use('/users', usersRoutes)
app.use('/admin', adminRoutes)
app.use('/item', itemRoutes)

module.exports = app
