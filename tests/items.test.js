const request = require('supertest')
const session = require('supertest-session')
const app = require('../app')

describe('Items', () => {
    let authentificatedSession
    let testSession = null

    beforeEach(() => {
        testSession = session(app)
    })

    beforeEach(done => {
        testSession
            .post("/users/login")
            .send({
                email: 'admin@admin.com',
                password: 'Adminapp'
            })
            .expect(302)
            .expect('Location', '/dashboard')
            .end((err) => {
                if (err) return done(err)
                authentificatedSession = testSession
                return done()
            })
    })

    it('Should insert new item', async (done) => {
        await authentificatedSession
            .post('/item/createItem')
            .send({
                ref: 'AA',
                name: 'vélo de test',
                quantity: 10,
                price: 1,
                selectSection: 1,
                selectShop: 1
            })
            .expect(302)
            .expect('Location', '/dashboard')
        done()
    })

    // it('Should Modify item', async (done) => {
    //     await authentificatedSession
    //         .post('/item/edit/AA')
    //         .send({
    //             name: 'Cuissard',
    //             quantity: 25,
    //             price: 40,
    //             selectSection: 1,
    //             selectShop: 1
    //         })
    //         .expect(302)
    //         .expect('Location', '/dashboard')
    //     done()
    // })

    it('Should delete item', async (done) => {
        await authentificatedSession
            .post('/item/delete/AA')
            .expect(302)
            .expect('Location', '/dashboard')
        done()
    })
})


