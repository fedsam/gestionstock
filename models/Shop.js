class Shop {
	// Constructor
	constructor(id, name, city) {
		this.id = id
		this.name = name
		this.city = city
	}
	// Getter
	get id() {
		return this.id
	}

	get name() {
		return this.name
	}

	get city() {
		return this.city
	}

	// Setter
	set id(id) {
		this.id = id
	}

	set name(name) {
		this.name = name
	}

	set city(city) {
		this.city = city
	}
}
